package Prestamo.Desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class Deposito extends JFrame {

	private JPanel contentPane;
	private JTextField txtDeposito;
	private JTextField textField_2;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Deposito frame = new Deposito();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Deposito() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 534, 401);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtDeposito = new JTextField();
		txtDeposito.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 30));
		txtDeposito.setText("Deposito");
		txtDeposito.setBounds(173, 11, 171, 39);
		contentPane.add(txtDeposito);
		txtDeposito.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Tipo");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		lblNewLabel.setBounds(70, 94, 86, 31);
		contentPane.add(lblNewLabel);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setFont(new Font("Arial", Font.PLAIN, 16));
		lblFecha.setBounds(70, 136, 86, 33);
		contentPane.add(lblFecha);
		
		JLabel lblMonto = new JLabel("Monto");
		lblMonto.setFont(new Font("Arial", Font.PLAIN, 16));
		lblMonto.setBounds(70, 236, 86, 33);
		contentPane.add(lblMonto);
		
		JLabel lblUsuarioId = new JLabel("Usuario ID");
		lblUsuarioId.setFont(new Font("Arial", Font.PLAIN, 16));
		lblUsuarioId.setBounds(70, 186, 86, 33);
		contentPane.add(lblUsuarioId);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(204, 186, 140, 33);
		contentPane.add(textField_2);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		comboBox.setBounds(204, 149, 34, 20);
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		comboBox_1.setBounds(248, 149, 43, 20);
		contentPane.add(comboBox_1);
		
		JLabel lblDa = new JLabel("Día");
		lblDa.setBounds(204, 136, 34, 14);
		contentPane.add(lblDa);
		
		JLabel lblMes = new JLabel("Mes\r\n");
		lblMes.setBounds(248, 136, 39, 14);
		contentPane.add(lblMes);
		
		JLabel lblAo = new JLabel("Año");
		lblAo.setBounds(297, 136, 47, 14);
		contentPane.add(lblAo);
		
		textField_1 = new JTextField();
		textField_1.setBounds(297, 149, 47, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 12));
		btnGuardar.setBounds(413, 306, 89, 23);
		contentPane.add(btnGuardar);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Prestamista", "Prestatario"}));
		comboBox_2.setFont(new Font("Arial", Font.PLAIN, 16));
		comboBox_2.setBounds(204, 94, 140, 31);
		contentPane.add(comboBox_2);
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setModel(new DefaultComboBoxModel(new String[] {"$100.000", "$150.000", "$200.000", "$250.000", "$300.000", "$350.000", "$400.000", "$450.000", "$500.000"}));
		comboBox_3.setFont(new Font("Arial", Font.PLAIN, 16));
		comboBox_3.setBounds(204, 232, 140, 41);
		contentPane.add(comboBox_3);
	}
}
