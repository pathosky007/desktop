package Prestamo.Desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.DropMode;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Region extends JFrame {

	private JPanel contentPane;
	private JLabel lblSeleccioneRegion;
	private JLabel lblRegin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Region frame = new Region();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Region() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 494, 232);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 12));
		btnNewButton.setBounds(379, 98, 89, 23);
		contentPane.add(btnNewButton);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Arial", Font.PLAIN, 16));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"XV Arica y Parinacota", "I Tarapacá", "II Antofagasta", "III Atacama", "IV Coquimbo", "V Valparaíso", "VI Libertador General Bernardo O’Higgins", "VII Maule", "VIII Bío-Bío", "IX La Araucanía", "XIV Los Ríos", "X Los Lagos", "XI Aysén del General Carlos Ibañez del Campo", "XII Magallanes y de la Antártica Chilena", "RM Metropolitana"}));
		comboBox.setBounds(172, 89, 170, 39);
		contentPane.add(comboBox);
		
		lblSeleccioneRegion = new JLabel("Seleccione region");
		lblSeleccioneRegion.setFont(new Font("Arial", Font.PLAIN, 16));
		lblSeleccioneRegion.setBounds(33, 90, 126, 37);
		contentPane.add(lblSeleccioneRegion);
		
		lblRegin = new JLabel("REGIÓN");
		lblRegin.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 30));
		lblRegin.setBounds(155, 11, 148, 45);
		contentPane.add(lblRegin);
	}
}
