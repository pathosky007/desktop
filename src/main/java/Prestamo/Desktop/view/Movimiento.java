package Prestamo.Desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.Color;

public class Movimiento extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Movimiento frame = new Movimiento();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Movimiento() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 437, 394);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMovimiento = new JLabel("Movimiento");
		lblMovimiento.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 30));
		lblMovimiento.setBounds(132, 11, 183, 41);
		contentPane.add(lblMovimiento);
		
		JLabel lblNewLabel = new JLabel("Tipo");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		lblNewLabel.setBounds(38, 75, 94, 41);
		contentPane.add(lblNewLabel);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Prestamista", "Prestatario"}));
		comboBox.setFont(new Font("Arial", Font.PLAIN, 16));
		comboBox.setBounds(158, 75, 140, 41);
		contentPane.add(comboBox);
		
		JLabel lblMonto = new JLabel("Monto");
		lblMonto.setFont(new Font("Arial", Font.PLAIN, 16));
		lblMonto.setBounds(38, 139, 94, 41);
		contentPane.add(lblMonto);
		
		JLabel lblUsuarioId = new JLabel("Usuario ID");
		lblUsuarioId.setFont(new Font("Arial", Font.PLAIN, 16));
		lblUsuarioId.setBounds(38, 194, 94, 41);
		contentPane.add(lblUsuarioId);
		
		JLabel lblPrestamoId = new JLabel("Prestamo ID");
		lblPrestamoId.setFont(new Font("Arial", Font.PLAIN, 16));
		lblPrestamoId.setBounds(38, 246, 94, 41);
		contentPane.add(lblPrestamoId);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"$100.000", "$150.000", "$200.000", "$250.000", "$300.000", "$350.000", "$400.000", "$450.000", "$500.000"}));
		comboBox_1.setFont(new Font("Arial", Font.PLAIN, 16));
		comboBox_1.setBounds(158, 139, 140, 41);
		contentPane.add(comboBox_1);
		
		textField = new JTextField();
		textField.setFont(new Font("Arial", Font.PLAIN, 16));
		textField.setBounds(158, 196, 140, 41);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Arial", Font.PLAIN, 16));
		textField_1.setColumns(10);
		textField_1.setBounds(158, 246, 140, 41);
		contentPane.add(textField_1);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(301, 306, 89, 23);
		contentPane.add(btnGuardar);
	}
}
