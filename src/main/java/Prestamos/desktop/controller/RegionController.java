package Prestamos.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Prestamo.Desktop.Model.RegionModelo;
import Prestamo.Desktop.view.RegionForm;

public class RegionController implements ActionListener{
	
	RegionForm rf;

	RegionController(RegionForm rf){
		this.rf = rf;
		rf.getBtnNewButton().addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(e.getSource()==rf.getBtnNewButton()) {
			String nombre = rf.getTextField().getText();
			
			if(!(nombre.equals(""))) {
				RegionModelo rm = new RegionModelo();
				rm.setNombre(nombre);
				if(rm.crear()) {
					JOptionPane.showMessageDialog(null, "Se registro con exito");
				}
			}else{
				JOptionPane.showMessageDialog(null, "Debe Ingresar algun campo");
			}
		}
		
		
	}
		
	
	
	
}
